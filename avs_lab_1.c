#include <stdio.h>
#include <math.h>
#include <stdbool.h>

bool parse_float(char str[], long double* result);

int main(int argc, char* argv[])
{
    long double a = 0, b = 0, c = 0;

    if (argc == 4) {
        if (!parse_float(argv[1], &a) || !parse_float(argv[2], &b) ||
            !parse_float(argv[3], &c)) {
            printf("Invalid argument(s) detected.\n");
            return -1;
        }
        if (a == 0) {
            printf("The equation is not a quadratic function.\n");
            return -2;
        }
    }
    else {
        printf("Wrong number of arguments.\n");
        return -3;
    }

    long double x1 = 0, x2 = 0;
    bool is_solved = false;
    bool has_overflowed = false;

    __asm__ (
        "finit\n\t"
        "fldt %[a]\n\t"             //st(0) = a
        "fadd %%st, %%st(0)\n\t"    //st(0) = 2*a
        "fadd %%st, %%st(0)\n\t"    //st(0) = 4*a
        "fldt %[c]\n\t"             //st(0) = c, st(1) = 4*a
        "fmulp\n\t"                 //st(0) = 4*a*c
        "fldt %[b]\n\t"             //st(0) = b, st(1) = 4*a*c
        "fmul %%st\n\t"             //st(0) = b^2, st(1) = 4*a*c
        "fsubp %%st(1)\n\t"         //st(0) = b^2 - 4*a*c = D
        "ftst\n\t"                  //Compare D with 0.
        "fstsw %%ax\n\t"
        "sahf\n\t"
        "jb exit\n\t"               //No solutions.
        "jo overflow\n\t"           //Overflow detected.
        "movb $1, %[is_solved]\n\t" //is_solved = true
        "fsqrt\n\t"                 //st(0) = sqrt(D)
        "fldt %[a]\n\t"             //st(0) = a, st(1) = sqrt(D)
        "fadd %%st, %%st(0)\n\t"    //st(0) = 2*a, st(1) = sqrt(D)
        "fxch\n\t"                  //st(0) = sqrt(D), st(1) = 2*a
        "fdiv %%st(1)\n\t"          //st(0) = sqrt(D)/(2*a), st(1) = 2*a
        "fldt %[b]\n\t"             //st(0) = b, st(1) = sqrt(D)/(2*a), 
                                    //st(2) = 2*a
        "fchs\n\t"                  //st(0) = -b, st(1) = sqrt(D)/(2*a), 
                                    //st(2) = 2*a
        "fdivp %%st(2)\n\t"         //st(0) = sqrt(D)/(2*a), st(1) = -b/(2*a)
        "fld %%st(0)\n\t"           //st(0) = sqrt(D)/(2*a), 
                                    //st(1) = sqrt(D)/(2*a), st(2) = -b/(2*a)
        "fsubr %%st(2), %%st\n\t"   //st(0) = (-b - sqrt(D))/(2*a) = x2, 
                                    //st(1) = sqrt(D)/(2*a), st(2) = -b/(2*a)
        "fxch %%st(2)\n\t"          //st(0) = -b/(2*a), 
                                    //st(1) = sqrt(D)/(2*a), st(2) = x2
        "faddp\n\t"                 //st(0) = (-b+ sqrt(D))/(2*a) = x1, 
                                    //st(1) = x2
        "jmp exit\n\t"
        "overflow:\n\t"
        "movb $1, %[has_overflowed]\n\t"
        "exit:\n\t"
        : [x1] "=&t" (x1), [x2] "=&u" (x2), [is_solved] "=m" (is_solved),
            [has_overflowed] "=m" (has_overflowed)
        : [a] "m" (a), [b] "m" (b), [c] "m"(c)
    );
    
    printf("a = %Lf\nb = %Lf\nc = %Lf\n", a, b, c);
    if (is_solved)
        printf("x1 = %Lf, x2 = %Lf.\n", x1, x2);
    else if (has_overflowed)
        printf("Sorry, overflow detected.\n");
    else
        printf("No solutions.\n");

    return 0;
}

bool parse_float(char str[], long double* result)
{
    int sign = 1;
    int position = 0;
    
    if (str[0] == '-') {
        sign = -1;
        position = 1;
    }

    char character;
    bool point_reached = false;
    long double numbers_after_point = 0.1;

    while ((character = str[position++]) != '\0') {
        if (character >= '0' && character <= '9') {
            if (!point_reached) 
                *result = *result * 10 + character - '0';
            else {
                *result += (character - 48) * numbers_after_point;
                numbers_after_point /= 10; 
            }
        }
        else if (character == '.' || character == ',')
            if (!point_reached)
                point_reached = true;
            else 
                return false;
        else
            return false;
    }
    if (isinf(*result) || isnan(*result))
        return false;
    *result *= sign;

    return true;
}
